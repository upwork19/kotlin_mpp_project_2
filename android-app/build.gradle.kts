plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
    id("com.bugsnag.android.gradle")
}

android {
    compileSdkVersion(28)

    defaultConfig {
        applicationId = "com.bugsnag.example.kotlinmp"
        minSdkVersion(21)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("debug") {
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    packagingOptions {
        // Ktor prevents the build otherwise
        pickFirst("META-INF/atomicfu.kotlin_module")
        pickFirst("META-INF/kotlinx-io.kotlin_module")
//        pickFirst("META-INF/ktor-http.kotlin_module")
        pickFirst("META-INF/ktor-*.kotlin_module")
        pickFirst("META-INF/klock.kotlin_module")
        pickFirst("META-INF/kotlinx-coroutines-io.kotlin_module")
        pickFirst("META-INF/kotlinx-coroutines-core.kotlin_module")
    }
    compileOptions {
        setSourceCompatibility(JavaVersion.VERSION_1_8)
        setTargetCompatibility(JavaVersion.VERSION_1_8)
    }
}

dependencies {
    api(project(":common"))

    // kotlin jdk
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}")

    // google android libs
    implementation("androidx.appcompat:appcompat:${Versions.androidx}")
    implementation("com.google.android.material:material:${Versions.androidx}")

    // crash reporting
    implementation("com.bugsnag:bugsnag-android:${Versions.bugsnagAndroid}")

    implementation ("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation ("com.github.bumptech.glide:glide:4.9.0")
}
